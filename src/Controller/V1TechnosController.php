<?php

namespace App\Controller;
use App\Repository\TechnosRepository;  // EntityRepo donnée utilisée

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1TechnosController extends AbstractController
{
    #[Route('/v1/technos', name: 'app_v1_technos')]
    public function index(TechnosRepository $technosRepository): Response
    {
        return $this->render('v1_technos/index.html.twig', [
            'technos' => $technosRepository->findAll(),
        ]);
    }
}
