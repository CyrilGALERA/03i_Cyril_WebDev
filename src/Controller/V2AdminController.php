<?php

namespace App\Controller;
use App\Repository\ProjetsRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V2AdminController extends AbstractController
{
    #[Route('/v2/admin', name: 'app_v2_admin')]
    public function index(ProjetsRepository $projetsRepository): Response
    {
        return $this->render('v2_admin/index.html.twig', [
            'projets' => $projetsRepository->findAll(),
        ]);
    }
    // #[Route('/', name: 'app_projets_index', methods: ['GET'])]
    // public function index(): Response
    // {
    //     return $this->render('projets/index.html.twig', [
            
    //     ]);
    // }

}
