<?php

namespace App\Controller;

use App\Repository\CompetencesRepository;  // EntityRepo donnée utilisée

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1CompetencesController extends AbstractController
{
    #[Route('/v1/competences', name: 'app_v1_competences')]
    public function index(CompetencesRepository $competencesRepository): Response
    {
        return $this->render('v1_competences/index.html.twig', [
            'competences' => $competencesRepository->findAll(),
        ]);
    }
}
