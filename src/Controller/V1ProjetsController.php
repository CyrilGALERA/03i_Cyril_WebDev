<?php

namespace App\Controller;

// use App\Entity\Projets;                // Entity donnée utilisée
use App\Repository\ProjetsRepository;  // EntityRepo donnée utilisée

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1ProjetsController extends AbstractController
{
    #[Route('/v1/projets', name: 'app_v1_projets')]
    public function index(ProjetsRepository $projetsRepository): Response
    {
        return $this->render('v1_projets/index.html.twig', [
            'projets' => $projetsRepository->findAll(),
        ]);
    }

}
