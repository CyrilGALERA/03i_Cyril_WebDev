<?php

namespace App\Controller;

use App\Entity\Langages;
use App\Form\LangagesType;
use App\Repository\LangagesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/langages')]
class LangagesController extends AbstractController
{
    #[Route('/', name: 'app_langages_index', methods: ['GET'])]
    public function index(LangagesRepository $langagesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('langages/index.html.twig', [
            'langages' => $langagesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_langages_new', methods: ['GET', 'POST'])]
    public function new(Request $request, LangagesRepository $langagesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $langage = new Langages();
        $form = $this->createForm(LangagesType::class, $langage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $langagesRepository->add($langage);
            return $this->redirectToRoute('app_langages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('langages/new.html.twig', [
            'langage' => $langage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_langages_show', methods: ['GET'])]
    public function show(Langages $langage): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('langages/show.html.twig', [
            'langage' => $langage,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_langages_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Langages $langage, LangagesRepository $langagesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $form = $this->createForm(LangagesType::class, $langage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $langagesRepository->add($langage);
            return $this->redirectToRoute('app_langages_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('langages/edit.html.twig', [
            'langage' => $langage,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_langages_delete', methods: ['POST'])]
    public function delete(Request $request, Langages $langage, LangagesRepository $langagesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        if ($this->isCsrfTokenValid('delete'.$langage->getId(), $request->request->get('_token'))) {
            $langagesRepository->remove($langage);
        }

        return $this->redirectToRoute('app_langages_index', [], Response::HTTP_SEE_OTHER);
    }
}
