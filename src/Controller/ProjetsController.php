<?php

namespace App\Controller;

use App\Entity\Projets;
use App\Form\ProjetsType;
use App\Repository\ProjetsRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/projets')]
class ProjetsController extends AbstractController
{
    #[Route('/', name: 'app_projets_index', methods: ['GET'])]
    public function index(ProjetsRepository $projetsRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('projets/index.html.twig', [
            'projets' => $projetsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_projets_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProjetsRepository $projetsRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $projet = new Projets();
        $form = $this->createForm(ProjetsType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetsRepository->add($projet);
            return $this->redirectToRoute('app_projets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projets/new.html.twig', [
            'projet' => $projet,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projets_show', methods: ['GET'])]
    public function show(Projets $projet): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('projets/show.html.twig', [
            'projet' => $projet,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_projets_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Projets $projet, ProjetsRepository $projetsRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $form = $this->createForm(ProjetsType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetsRepository->add($projet);
            return $this->redirectToRoute('app_projets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projets/edit.html.twig', [
            'projet' => $projet,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_projets_delete', methods: ['POST'])]
    public function delete(Request $request, Projets $projet, ProjetsRepository $projetsRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        if ($this->isCsrfTokenValid('delete'.$projet->getId(), $request->request->get('_token'))) {
            $projetsRepository->remove($projet);
        }

        return $this->redirectToRoute('app_projets_index', [], Response::HTTP_SEE_OTHER);
    }
}
