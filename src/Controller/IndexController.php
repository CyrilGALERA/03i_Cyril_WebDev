<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
    
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }
}
