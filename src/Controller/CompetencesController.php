<?php

namespace App\Controller;

use App\Entity\Competences;
use App\Form\CompetencesType;
use App\Repository\CompetencesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/competences')]
class CompetencesController extends AbstractController
{
    #[Route('/', name: 'app_competences_index', methods: ['GET'])]
    public function index(CompetencesRepository $competencesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('competences/index.html.twig', [
            'competences' => $competencesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_competences_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CompetencesRepository $competencesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $competence = new Competences();
        $form = $this->createForm(CompetencesType::class, $competence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $competencesRepository->add($competence);
            return $this->redirectToRoute('app_competences_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('competences/new.html.twig', [
            'competence' => $competence,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_competences_show', methods: ['GET'])]
    public function show(Competences $competence): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        return $this->render('competences/show.html.twig', [
            'competence' => $competence,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_competences_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Competences $competence, CompetencesRepository $competencesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        $form = $this->createForm(CompetencesType::class, $competence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $competencesRepository->add($competence);
            return $this->redirectToRoute('app_competences_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('competences/edit.html.twig', [
            'competence' => $competence,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_competences_delete', methods: ['POST'])]
    public function delete(Request $request, Competences $competence, CompetencesRepository $competencesRepository): Response
    {
        // Privatisation des acces: 
        // Cas 1 Visiteur-User: par défaut s'il se /register le visiter sera un user de role 'ROLE_USER'  par défaut 
        // Cas 1 Visiteur-User: $this->denyAccessUnlessGranted('ROLE_USER'); 
        $this->denyAccessUnlessGranted('ROLE_USER');    // control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        
        if ($this->isCsrfTokenValid('delete'.$competence->getId(), $request->request->get('_token'))) {
            $competencesRepository->remove($competence);
        }

        return $this->redirectToRoute('app_competences_index', [], Response::HTTP_SEE_OTHER);
    }
}
