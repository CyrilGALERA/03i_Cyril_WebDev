<?php

namespace App\Controller;

use App\Repository\LangagesRepository;  // EntityRepo donnée utilisée

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1LangagesController extends AbstractController
{
    #[Route('/v1/langages', name: 'app_v1_langages')]
    public function index(LangagesRepository $langagesRepository): Response
    {
        return $this->render('v1_langages/index.html.twig', [
            'langages' => $langagesRepository->findAll(),
        ]);
    }
}
